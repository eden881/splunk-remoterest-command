# splunk-remoterest-command

This Splunk app contains a custom search command that allows to read a REST API
endpoint of a remote Splunk instance using an authentication token.

It intends to mimic he behaviour of the built-in `rest` SPL command.

The command uses Python 3 syntax and should be cross-compatible with Python 2, as it
uses the `__future__` module. However, it is currently tested on Splunk 8.0.6 only.
