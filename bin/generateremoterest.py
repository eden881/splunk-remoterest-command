from __future__ import absolute_import, division, print_function, unicode_literals
import sys
import os
import time
import requests

splunkhome = os.environ['SPLUNK_HOME']
sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..", "lib"))

from splunklib.searchcommands import dispatch, GeneratingCommand, Configuration, Option, validators

@Configuration(type='reporting')
class GenerateRemoteRestCommand(GeneratingCommand):

    server = Option(require=True)
    token = Option(require=True)
    endpoint = Option(require=True)

    def generate(self):

        # Initialize variables
        endpoint = self.endpoint.strip('/')
        server = self.server
        token = self.token
        request_headers = {"Authorization": "Bearer %s" % (token)}
        danger_chars = r'?#=^&*+%'

        # Stop execution if dangerous characters are detected in the input
        if any(danger_char in s for danger_char in danger_chars for s in (endpoint, server)):
            self.logger.critical("Dangerous characters detected")
            sys.exit(3)

        # Execute the request
        response = requests.get('https://%s:8089/%s?output_mode=json' % (server, endpoint), headers=request_headers, verify=False)

        # Stop execution if the response status is not 200
        if response.status_code != 200:
            self.logger.critical("Something went wrong: " + str(response))
            sys.exit(3)

        current_time = time.time()

        # Prepare each entry in the response and yield it
        for entry in response.json()["entry"]:
            self.clean_entry(entry, ["links", "eai:acl"])
            payload = self.flatten_dict(entry)
            payload["_time"] = current_time
            payload["splunk_server"] = server

            yield payload

    # Used to convert dictionaries that contain nested dictionaries to flat
    # Returns the result
    def flatten_dict(self, d: dict) -> dict:

        result = {}

        for k, v in d.items():
            if isinstance(v, dict):
                for k_inner, v_inner in self.flatten_dict(v).items():
                    result['.'.join([k, k_inner])] = v_inner
            else:
                result[k] = v

        return result

    # Used to filter out some common unwanted fields
    # Does not return the result; Mutates the input dictionary
    def clean_entry(self, d: dict, keys: list):

        for k in keys:
            try:
                del d[k]
            except KeyError:
                pass

        for v in d.values():
            if isinstance(v, dict):
                self.clean_entry(v, keys)

dispatch(GenerateRemoteRestCommand, sys.argv, sys.stdin, sys.stdout, __name__)
